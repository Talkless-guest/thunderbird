<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY aboutTelemetry.pingDataSource "Ping datakilde:">
<!ENTITY aboutTelemetry.showCurrentPingData "Nuværende ping-data">
<!ENTITY aboutTelemetry.showArchivedPingData "Arkiverede ping-data">
<!ENTITY aboutTelemetry.showSubsessionData "Vis data fra undersession">
<!ENTITY aboutTelemetry.choosePing "Vælg ping:">
<!ENTITY aboutTelemetry.archivePingType "Ping-type">
<!ENTITY aboutTelemetry.archivePingHeader "Ping">
<!ENTITY aboutTelemetry.optionGroupToday "I dag">
<!ENTITY aboutTelemetry.optionGroupYesterday "I går">
<!ENTITY aboutTelemetry.optionGroupOlder "Ældre">
<!ENTITY aboutTelemetry.payloadChoiceHeader "Payload">
<!-- LOCALIZATION NOTE(aboutTelemetry.previousPing, aboutTelemetry.nextPing):
	These strings are displayed when selecting Archived pings, and they’re
	used to move to the next or previous ping. -->
<!ENTITY aboutTelemetry.previousPing "&lt;&lt;">
<!ENTITY aboutTelemetry.nextPing "&gt;&gt;">

<!ENTITY aboutTelemetry.pageTitle "Telemetry-data">
<!ENTITY aboutTelemetry.moreInformations "Leder du efter mere information?">
<!ENTITY aboutTelemetry.firefoxDataDoc "<a>Firefox Data Documentation</a> indeholder guider om, hvordan du kan arbejde med vores dataværktøjer">
<!ENTITY aboutTelemetry.telemetryClientDoc "<a>Firefox Telemetry client documentation</a> indeholder definitioner af koncepter, API-dokumentation og data-referencer.">
<!ENTITY aboutTelemetry.telemetryDashboard "<a>Telemetry dashboards</a> giver dig mulighed for at visualisere de data, Mozilla modtager via Telemetry.">

<!ENTITY aboutTelemetry.showInFirefoxJsonViewer "Åbn i JSON-vieweren">

<!ENTITY aboutTelemetry.homeSection "Start">
<!ENTITY aboutTelemetry.generalDataSection "Generelle data">
<!ENTITY aboutTelemetry.environmentDataSection "Miljø-data">
<!ENTITY aboutTelemetry.sessionInfoSection "Sessionsoplysninger">
<!ENTITY aboutTelemetry.scalarsSection "Skalarer">
<!ENTITY aboutTelemetry.keyedScalarsSection "Indtastede skalarer">
<!ENTITY aboutTelemetry.histogramsSection "Histogrammer">
<!ENTITY aboutTelemetry.keyedHistogramsSection "Indtastede histogrammer">
<!ENTITY aboutTelemetry.eventsSection "Begivenheder">
<!ENTITY aboutTelemetry.simpleMeasurementsSection "Simple målinger">
<!ENTITY aboutTelemetry.telemetryLogSection "Telemetry-log">
<!ENTITY aboutTelemetry.slowSqlSection "Langsomme SQL-statements">
<!ENTITY aboutTelemetry.chromeHangsSection "Browser-frysninger">
<!ENTITY aboutTelemetry.addonDetailsSection "Detaljer for tilføjelser">
<!ENTITY aboutTelemetry.capturedStacksSection "Stakke">
<!ENTITY aboutTelemetry.lateWritesSection "Sene skrivninger">
<!ENTITY aboutTelemetry.rawPayloadSection "Råt payload">
<!ENTITY aboutTelemetry.raw "Rå JSON">

<!ENTITY aboutTelemetry.fullSqlWarning "Bemærk: Langsom SQL-debugging er aktiveret. Hele SQL-strenge vil måske være vist nedenfor, men de bliver ikke sendt til Telemetry. ">
<!ENTITY aboutTelemetry.fetchStackSymbols "Hent navne for funktioner for stakke">
<!ENTITY aboutTelemetry.hideStackSymbols "Vis rå data fra stakke">
