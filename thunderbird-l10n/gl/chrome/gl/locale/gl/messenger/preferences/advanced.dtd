<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY itemGeneral.label       "Xeral">
<!ENTITY dataChoicesTab.label    "Escolla de datos">
<!ENTITY itemUpdate.label        "Actualizar">
<!ENTITY itemNetworking.label    "Rede e espazo no disco">
<!ENTITY itemCertificates.label  "Certificados">

<!-- General Settings -->
<!ENTITY enableGlodaSearch.label       "Activar indexador de mensaxes">
<!ENTITY enableGlodaSearch.accesskey   "i">
<!ENTITY allowHWAccel.label            "Usar a aceleración por hardware cando estea dispoñíbel">
<!ENTITY allowHWAccel.accesskey        "h">
<!ENTITY storeType.label               "Tipo de almacenamento de mensaxes para as novas contas:">
<!ENTITY storeType.accesskey           "T">
<!ENTITY mboxStore2.label              "Un ficheiro por cartafol (mbox)">
<!ENTITY maildirStore.label            "Un ficheiro por mensaxe (maildir)">

<!ENTITY scrolling.label               "Desprazamento">
<!ENTITY useAutoScroll.label           "Utilizar desprazamento automático">
<!ENTITY useAutoScroll.accesskey       "U">
<!ENTITY useSmoothScrolling.label      "Utilizar desprazamento suave">
<!ENTITY useSmoothScrolling.accesskey  "m">

<!ENTITY systemIntegration.label       "Integración co sistema">
<!ENTITY alwaysCheckDefault.label      "Sempre comprobar se &brandShortName; é o navegador predeterminado no inicio">
<!ENTITY alwaysCheckDefault.accesskey  "a">
<!ENTITY searchIntegration.label       "Permitir que &searchIntegration.engineName; busque nas mensaxes">
<!ENTITY searchIntegration.accesskey   "P">
<!ENTITY checkDefaultsNow.label        "Comprobar agora…">
<!ENTITY checkDefaultsNow.accesskey    "C">
<!ENTITY configEditDesc.label          "Configuración avanzada">
<!ENTITY configEdit.label              "Editor de configuración…">
<!ENTITY configEdit.accesskey          "n">
<!ENTITY returnReceiptsInfo.label      "Determine como debe xestionar &brandShortName; os avisos de recepción.">
<!ENTITY showReturnReceipts.label      "Avisos de recepción…">
<!ENTITY showReturnReceipts.accesskey  "r">

<!-- Data Choices -->
<!ENTITY telemetrySection.label          "Telemetría">
<!ENTITY telemetryDesc.label             "Comparta datos de rendemento, uso, hardware e personalizacións sobre o seu cliente de correo con &vendorShortName; para axudarnos a mellorar &brandShortName;">
<!ENTITY enableTelemetry.label           "Activar a telemetría">
<!ENTITY enableTelemetry.accesskey       "T">
<!ENTITY telemetryLearnMore.label        "Máis información">

<!ENTITY crashReporterSection.label      "Informador de erros">
<!ENTITY crashReporterDesc.label         "&brandShortName; envía informes de erro para axudar a &vendorShortName; a facer o seu cliente de correo máis estábel  e seguro">
<!ENTITY enableCrashReporter.label       "Activar o informador de erros">
<!ENTITY enableCrashReporter.accesskey   "c">
<!ENTITY crashReporterLearnMore.label    "Máis información">

<!-- Update -->
<!-- LOCALIZATION NOTE (updateApp.label):
  Strings from aboutDialog.dtd are displayed in this section of the preferences.
  Please check for possible accesskey conflicts.
-->
<!-- LOCALIZATION NOTE (updateApp.version.*): updateApp.version.pre is
  followed by a version number, keep the trailing space or replace it with
  a different character as needed. updateApp.version.post is displayed after
  the version number, and is empty on purpose for English. You can use it
  if required by your language.
 -->
<!ENTITY updateAuto.label                "Instalar actualizacións automaticamente (recomendado: mellora a seguranza)">
<!ENTITY updateAuto.accesskey            "a">
<!ENTITY updateCheck.label               "Buscar actualizacións, pero permitirme escoller se quero instalalas">
<!ENTITY updateCheck.accesskey           "c">
<!ENTITY updateManual.label              "Non buscar actualizacións (non recomendado: risco de seguranza)">
<!ENTITY updateManual.accesskey          "N">
<!ENTITY updateHistory.label             "Amosar o historial de actualizacións">
<!ENTITY updateHistory.accesskey         "h">

<!ENTITY useService.label                "Usar un servizo en segundo plano para instalar as actualizacións">
<!ENTITY useService.accesskey            "z">

<!-- Networking and Disk Space -->
<!ENTITY showSettings.label            "Configuración…">
<!ENTITY showSettings.accesskey        "f">
<!ENTITY proxiesConfigure.label        "Configurar o modo en que &brandShortName; se conecta á Internet">
<!ENTITY connectionsInfo.caption       "Conexión">
<!ENTITY offlineInfo.caption           "Sen conexión">
<!ENTITY offlineInfo.label             "Configurar opcións sen conexión">
<!ENTITY showOffline.label             "Sen conexión…">
<!ENTITY showOffline.accesskey         "S">

<!ENTITY Diskspace                       "Espazo en disco">
<!ENTITY offlineCompactFolders.label     "Compactar todos os cartafoles ao liberar">
<!ENTITY offlineCompactFolders.accesskey "a">
<!ENTITY offlineCompactFoldersMB.label   "MB en total">

<!-- LOCALIZATION NOTE:
  The entities useCacheBefore.label and useCacheAfter.label appear on a single
  line in preferences as follows:

  &useCacheBefore.label  [ textbox for cache size in MB ]   &useCacheAfter.label;
-->
<!ENTITY useCacheBefore.label            "Usar ata">
<!ENTITY useCacheBefore.accesskey        "U">
<!ENTITY useCacheAfter.label             "MB de espazo en caché">
<!ENTITY clearCacheNow.label             "Borrar agora">
<!ENTITY clearCacheNow.accesskey         "L">

<!-- Certificates -->
<!ENTITY certSelection.description       "Cando un servidor solicite o meu certificado persoal:">
<!ENTITY certs.auto                      "Escoller un automaticamente">
<!ENTITY certs.auto.accesskey            "E">
<!ENTITY certs.ask                       "Preguntarme cada vez">
<!ENTITY certs.ask.accesskey             "P">
<!ENTITY enableOCSP.label                "Consultar aos servidores OCSP responder para confirmar a validez dos certificados">
<!ENTITY enableOCSP.accesskey            "t">

<!ENTITY viewSecurityDevices.label "Dispositivos de seguranza">
<!ENTITY viewSecurityDevices.accesskey "s">

<!ENTITY dateTimeFormatting.label      "Date and Time Formatting">
<!ENTITY updateApp2.label                "&brandShortName; Updates">
<!ENTITY updateApp.version.pre           "Version ">
<!ENTITY updateApp.version.post          "">
<!ENTITY overrideSmartCacheSize.label    "Override automatic cache management">
<!ENTITY overrideSmartCacheSize.accesskey "v">
<!ENTITY manageCertificates.label "Manage Certificates">
<!ENTITY manageCertificates.accesskey "M">
