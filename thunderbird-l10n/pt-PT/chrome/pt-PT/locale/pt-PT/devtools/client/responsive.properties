# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE These strings are used inside the Responsive Design Mode,
# available from the Web Developer sub-menu -> 'Responsive Design Mode'.
#
# The correct localization of this file might be to keep it in
# English, or another language commonly spoken among web developers.
# You want to make that choice consistent across the developer tools.
# A good criteria is the language in which you'd find the best
# documentation on web development on the web.

# LOCALIZATION NOTE (responsive.editDeviceList): option displayed in the device
# selector
responsive.editDeviceList=Editar lista…

# LOCALIZATION NOTE (responsive.exit): tooltip text of the exit button.
responsive.exit=Fechar modo de design responsivo

# LOCALIZATION NOTE (responsive.rotate): tooltip text of the rotate button.
responsive.rotate=Rodar viewport

# LOCALIZATION NOTE (responsive.deviceListLoading): placeholder text for
# device selector when it's still fetching devices
responsive.deviceListLoading=A carregar…

# LOCALIZATION NOTE (responsive.deviceListError): placeholder text for
# device selector when an error occurred
responsive.deviceListError=Nenhuma lista disponível

# LOCALIZATION NOTE (responsive.done): button text in the device list modal
responsive.done=Feito

# LOCALIZATION NOTE (responsive.noDeviceSelected): placeholder text for the
# device selector
responsive.noDeviceSelected=nenhum dispositivo selecionado

# LOCALIZATION NOTE  (responsive.title): the title displayed in the global
# toolbar
responsive.title=Modo de design responsivo

# LOCALIZATION NOTE (responsive.enableTouch): tooltip text for the touch
# simulation button when it's disabled
responsive.enableTouch=Ativar simulação de toque

# LOCALIZATION NOTE (responsive.disableTouch): tooltip text for the touch
# simulation button when it's enabled
responsive.disableTouch=Desativar simulação de toque

# LOCALIZATION NOTE  (responsive.screenshot): tooltip of the screenshot button.
responsive.screenshot=Tirar uma captura de ecrã do viewport

# LOCALIZATION NOTE (responsive.screenshotGeneratedFilename): The auto generated
# filename.
# The first argument (%1$S) is the date string in yyyy-mm-dd format and the
# second argument (%2$S) is the time string in HH.MM.SS format.
responsive.screenshotGeneratedFilename=Captura de ecrã %1$S às %2$S

# LOCALIZATION NOTE (responsive.remoteOnly): Message displayed in the tab's
# notification box if a user tries to open Responsive Design Mode in a
# non-remote tab.
responsive.remoteOnly=O modo de design responsivo está apenas disponível para separadores de navegação remotos, como os que são utilizados para conteúdo web no multi-processo do Firefox.

# LOCALIZATION NOTE (responsive.noContainerTabs): Message displayed in the tab's
# notification box if a user tries to open Responsive Design Mode in a
# container tab.
responsive.noContainerTabs=O modo de design responsivo está atualmente indisponível em separadores contentores.

# LOCALIZATION NOTE (responsive.noThrottling): UI option in a menu to configure
# network throttling.  This option is the default and disables throttling so you
# just have normal network conditions.  There is not very much room in the UI
# so a short string would be best if possible.
responsive.noThrottling=Sem limitações

# LOCALIZATION NOTE (responsive.changeDevicePixelRatio): tooltip for the
# device pixel ratio dropdown when is enabled.
responsive.changeDevicePixelRatio=Alterar o rácio do pixel do dispositivo do viewport

# LOCALIZATION NOTE (responsive.devicePixelRatio.auto): tooltip for the device pixel ratio
# dropdown when it is disabled because a device is selected.
# The argument (%1$S) is the selected device (e.g. iPhone 6) that set
# automatically the device pixel ratio value.
responsive.devicePixelRatio.auto=Rácio do pixel do dispositivo definido automaticamente para %1$S

# LOCALIZATION NOTE (responsive.customDeviceName): Default value in a form to
# add a custom device based on an arbitrary size (no association to an existing
# device).
responsive.customDeviceName=Dispositivo personalizado

# LOCALIZATION NOTE (responsive.customDeviceNameFromBase): Default value in a
# form to add a custom device based on the properties of another.  %1$S is the
# name of the device we're staring from, such as "Apple iPhone 6".
responsive.customDeviceNameFromBase=%1$S (Personalizado)

# LOCALIZATION NOTE (responsive.addDevice): Button text that reveals a form to
# be used for adding custom devices.
responsive.addDevice=Adicionar dispositivo

# LOCALIZATION NOTE (responsive.deviceAdderName): Label of form field for the
# name of a new device.  The available width is very low, so you might see
# overlapping text if the length is much longer than 5 or so characters.
responsive.deviceAdderName=Nome

# LOCALIZATION NOTE (responsive.deviceAdderSize): Label of form field for the
# size of a new device.  The available width is very low, so you might see
# overlapping text if the length is much longer than 5 or so characters.
responsive.deviceAdderSize=Tamanho

# LOCALIZATION NOTE (responsive.deviceAdderPixelRatio): Label of form field for
# the device pixel ratio of a new device.  The available width is very low, so you
# might see overlapping text if the length is much longer than 5 or so
# characters.
responsive.deviceAdderPixelRatio=DPR

# LOCALIZATION NOTE (responsive.deviceAdderUserAgent): Label of form field for
# the user agent of a new device.  The available width is very low, so you might
# see overlapping text if the length is much longer than 5 or so characters.
responsive.deviceAdderUserAgent=AU

# LOCALIZATION NOTE (responsive.deviceAdderTouch): Label of form field for the
# touch input support of a new device.  The available width is very low, so you
# might see overlapping text if the length is much longer than 5 or so
# characters.
responsive.deviceAdderTouch=Toque

# LOCALIZATION NOTE (responsive.deviceAdderSave): Button text that submits a
# form to add a new device.
responsive.deviceAdderSave=Guardar

# LOCALIZATION NOTE (responsive.deviceDetails): Tooltip that appears when
# hovering on a device in the device modal.  %1$S is the width of the device.
# %2$S is the height of the device.  %3$S is the device pixel ratio value of the
# device.  %4$S is the user agent of the device.  %5$S is a boolean value
# noting whether touch input is supported.
responsive.deviceDetails=Tamanho: %1$S x %2$S\nDPR: %3$S\nAU: %4$S\nToque: %5$S

# LOCALIZATION NOTE (responsive.devicePixelRatioOption): UI option in a menu to configure
# the device pixel ratio. %1$S is the devicePixelRatio value of the device.
responsive.devicePixelRatioOption=DPR: %1$S

# LOCALIZATION NOTE (responsive.reloadConditions.label): Label on button to open a menu
# used to choose whether to reload the page automatically when certain actions occur.
responsive.reloadConditions.label=Recarregar quando…

# LOCALIZATION NOTE (responsive.reloadConditions.title): Title on button to open a menu
# used to choose whether to reload the page automatically when certain actions occur.
responsive.reloadConditions.title=Escolha se recarregar a página automaticamente quando certas ações ocorrem

# LOCALIZATION NOTE (responsive.reloadConditions.touchSimulation): Label on checkbox used
# to select whether to reload when touch simulation is toggled.
responsive.reloadConditions.touchSimulation=Recarregar quando a simulação de toque é alternada

# LOCALIZATION NOTE (responsive.reloadConditions.userAgent): Label on checkbox used
# to select whether to reload when user agent is changed.
responsive.reloadConditions.userAgent=Recarregar quando o user agent é alterado

# LOCALIZATION NOTE (responsive.reloadNotification.description): Text in notification bar
# shown on first open to clarify that some features need a reload to apply.  %1$S is the
# label on the reload conditions menu (responsive.reloadConditions.label).
responsive.reloadNotification.description=Alterações de simulação de dispositivo requerem uma recarga para se aplicarem completamente.  Recargas automáticas estão desativadas por predefinição para evitar perder quaisquer alterações nas DevTools.  Pode ativar a recarga via menu “%1$S”.
