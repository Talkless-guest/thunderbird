# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE(pageSubtitle):
# - %1$S is replaced by the value of the toolkit.telemetry.server_owner preference
# - %2$S is replaced by brandFullName
pageSubtitle = 이 페이지는 텔레메트리에 의해서 수집된 성능, 하드웨어, 사용 상황, 맞춤 기능에 대한 정보를 표시합니다. 이 정보는 %2$S의 개선을 위해 %1$S에 송신됩니다.

# LOCALIZATION NOTE(homeExplanation):
# - %1$S is either telemetryEnabled or telemetryDisabled
# - %2$S is either extendedTelemetryEnabled or extendedTelemetryDisabled
homeExplanation = 텔레메트리는 %1$S이고 확장된 텔레메트리는 %2$S입니다.
telemetryEnabled = 활성화
telemetryDisabled = 비활성화
extendedTelemetryEnabled = 활성화
extendedTelemetryDisabled = 비활성화

# LOCALIZATION NOTE(settingsExplanation):
# - %1$S is either releaseData or prereleaseData
# - %2$S is either telemetryUploadEnabled or telemetryUploadDisabled
settingsExplanation = 텔레메트리가 %1$S를 모으고 있고 업로드는 %2$S입니다.
releaseData = 공개된 데이타
prereleaseData = 공개전 데이타
telemetryUploadEnabled = 활성화됨
telemetryUploadDisabled = 비활성화됨

# LOCALIZATION NOTE(pingDetails):
# - %1$S is replaced by a link with pingExplanationLink as text
# - %2$S is replaced by namedPing
pingDetails = 각 정보 조각들은 “%1$S“로 번들되어 보내집니다. 지금은 %2$S 핑을 보고 있습니다.
# LOCALIZATION NOTE(namedPing):
# - %1$S is replaced by the ping localized timestamp, e.g. “2017/07/08 10:40:46”
# - %2$S is replaced by the ping name, e.g. “saved-session”
namedPing = %1$S, %2$S
# LOCALIZATION NOTE(pingDetailsCurrent):
# - %1$S is replaced by a link with pingExplanationLink as text
# - %2$S is replaced by currentPing
pingDetailsCurrent = 각 정보 조각들은 “%1$S“로 번들되어 보내집니다. 지금은 %2$S 핑을 보고 있습니다.
pingExplanationLink = 핑
currentPing = 현재

# LOCALIZATION NOTE(filterPlaceholder): string used as a placeholder for the
# search field, %1$S is replaced by the section name from the structure of the
# ping. More info about it can be found here:
# https://firefox-source-docs.mozilla.org/toolkit/components/telemetry/telemetry/data/main-ping.html
filterPlaceholder = %1$S에서 찾기
filterAllPlaceholder = 모든 섹션에서 찾기

# LOCALIZATION NOTE(resultsForSearch): %1$S is replaced by the searched terms
resultsForSearch = “%1$S”에 대한 결과
# LOCALIZATION NOTE(noSearchResults):
# - %1$S is replaced by the section name from the structure of the ping.
# More info about it can be found here: https://firefox-source-docs.mozilla.org/toolkit/components/telemetry/telemetry/data/main-ping.html
# - %2$S is replaced by the current text in the search input
noSearchResults = 죄송합니다! %1$S에 “%2$S”에 대한 결과가 없습니다.
# LOCALIZATION NOTE(noSearchResultsAll): %S is replaced by the searched terms
noSearchResultsAll = 죄송합니다! “%S”에 대한 섹션 결과가 없습니다.
# LOCALIZATION NOTE(noDataToDisplay): %S is replaced by the section name.
# This message is displayed when a section is empty.
noDataToDisplay = 죄송합니다! “%S”에 현재 데이터가 없습니다.
# LOCALIZATION NOTE(currentPingSidebar): used as a tooltip for the “current”
# ping title in the sidebar
currentPingSidebar = 현재 핑
# LOCALIZATION NOTE(telemetryPingTypeAll): used in the “Ping Type” select
telemetryPingTypeAll = 모두

# LOCALIZATION NOTE(histogram*): these strings are used in the “Histograms” section
histogramSamples = 샘플 
histogramAverage = 평균
histogramSum = 합
# LOCALIZATION NOTE(histogramCopy): button label to copy the histogram
histogramCopy = 복사

# LOCALIZATION NOTE(telemetryLog*): these strings are used in the “Telemetry Log” section
telemetryLogTitle = 수집 기능 로그
telemetryLogHeadingId = 식별자
telemetryLogHeadingTimestamp = 타임스탬프
telemetryLogHeadingData = 데이터

# LOCALIZATION NOTE(slowSql*): these strings are used in the “Slow SQL Statements” section
slowSqlMain = 주요 쓰레드에서 느린 SQL문
slowSqlOther = 헬퍼 쓰레드에서 느린 SQL문
slowSqlHits = 횟수
slowSqlAverage = 평균 시간(ms)
slowSqlStatement = 문장

# LOCALIZATION NOTE(histogram*): these strings are used in the “Add-on Details” section
addonTableID = 부가 기능 ID
addonTableDetails = 상세
# LOCALIZATION NOTE(addonProvider):
# - %1$S is replaced by the name of an Add-on Provider (e.g. “XPI”, “Plugin”)
addonProvider = %1$S 제작

keysHeader = 프로퍼티
namesHeader = 이름
valuesHeader = 값

# LOCALIZATION NOTE(chrome-hangs-title):
# - %1$S is replaced by the number of the hang
# - %2$S is replaced by the duration of the hang
chrome-hangs-title = 멈춤 리포트 #%1$S (%2$S초)
# LOCALIZATION NOTE(captured-stacks-title):
# - %1$S is replaced by the string key for this stack
# - %2$S is replaced by the number of times this stack was captured
captured-stacks-title = %1$S (캡쳐 수: %2$S)
# LOCALIZATION NOTE(late-writes-title):
# - %1$S is replaced by the number of the late write
late-writes-title = 최종 작성- #%1$S번

stackTitle = 스택:
memoryMapTitle = 메모리 맵:

errorFetchingSymbols = 심볼을 가져오는데 오류가 생겼습니다. 인터넷 연결을 확인해 보시고 다시 시도해 주십시오.

parentPayload = 부모 페이로드
# LOCALIZATION NOTE(childPayloadN):
# - %1$S is replaced by the number of the child payload (e.g. “1”, “2”)
childPayloadN = 자식 페이로드 %1$S
timestampHeader = 타임스탬프
categoryHeader = 카테고리
methodHeader = 메서드
objectHeader = 객체
extraHeader = 추가
