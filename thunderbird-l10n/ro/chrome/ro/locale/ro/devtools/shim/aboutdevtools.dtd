<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- LOCALIZATION NOTE : This file contains the strings used in aboutdevtools.xhtml,
  -  displayed when going to about:devtools. UI depends on the value of the preference
  -  "devtools.enabled".
  -
  -  "aboutDevtools.enable.*" and "aboutDevtools.newsletter.*" keys are used when DevTools
     are disabled
  -  "aboutDevtools.welcome.*" keys are used when DevTools are enabled
  - -->

<!-- LOCALIZATION NOTE (aboutDevtools.headTitle): Text of the title tag for about:devtools -->
<!ENTITY  aboutDevtools.headTitle "Despre instrumentele de dezvoltare">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.title): Title of the top about:devtools
  -  section displayed when DevTools are disabled. -->
<!ENTITY  aboutDevtools.enable.title "Activează instrumentele de dezvoltare Firefox">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementTitle): Title of the top
  -  section displayed when devtools are disabled and the user triggered DevTools by using
  -  the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementTitle "Activează instrumentele de dezvoltare Firefox pentru a folosi Inspectează elementul">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementMessage): Message displayed
  -  when users come from using the Inspect Element menu item. -->

<!-- LOCALIZATION NOTE (aboutDevtools.enable.aboutDebuggingMessage): Message displayed
  -  when users come from about:debugging. -->

<!-- LOCALIZATION NOTE (aboutDevtools.enable.keyShortcutMessage): Message displayed when
  -  users pressed a DevTools key shortcut. -->

<!-- LOCALIZATION NOTE (aboutDevtools.enable.menuMessage2): Message displayed when users
  -  clicked on a "Enable Developer Tools" menu item. -->

<!-- LOCALIZATION NOTE (aboutDevtools.enable.commonMessage): Generic message displayed for
  -  all possible entry points (keyshortcut, menu item etc…). -->

<!-- LOCALIZATION NOTE (aboutDevtools.enable.learnMoreLink): Text for the link to
  -  https://developer.mozilla.org/docs/Tools displayed in the top section when DevTools
  -  are disabled. -->
<!ENTITY  aboutDevtools.enable.learnMoreLink "Află mai multe despre instrumentele de dezvoltare">

<!ENTITY  aboutDevtools.enable.enableButton "Activează instrumentele de dezvoltare">
<!ENTITY  aboutDevtools.enable.closeButton2 "Închide această filă">

<!ENTITY  aboutDevtools.welcome.title "Bun venit la instrumentele de dezvoltare Firefox!">

<!ENTITY  aboutDevtools.newsletter.title "Buletinul de știri Mozilla pentru dezvoltatori">
<!-- LOCALIZATION NOTE (aboutDevtools.newsletter.message): Subscribe form message.
  -  The newsletter is only available in english at the moment.
  -  See Bug 1415273 for support of additional languages.-->
<!ENTITY  aboutDevtools.newsletter.message "Obține știri pentru dezvoltatori, trucuri și resurse direct în căsuța poștală.">
<!ENTITY  aboutDevtools.newsletter.email.placeholder "E-mail">
<!ENTITY  aboutDevtools.newsletter.privacy.label "Sunt de acord ca Mozilla să îmi prelucreze informațiile așa cum este explicat în această <a class='external' href='https://www.mozilla.org/privacy/'>Politică de confidențialitate</a>.">
<!ENTITY  aboutDevtools.newsletter.subscribeButton "Abonează-te">
<!ENTITY  aboutDevtools.newsletter.thanks.title "Îți mulțumim!">
<!ENTITY  aboutDevtools.newsletter.thanks.message "Dacă nu ai confirmat anterior o abonare la un buletin de știri care are legătură cu Mozilla, este posibil să fie nevoie să o faci. Te rugăm să verifici mesajele primite sau dosarul spam pentru a găsi e-mailul de la noi.">

<!ENTITY  aboutDevtools.footer.title "Firefox Developer Edition">
<!ENTITY  aboutDevtools.footer.message "Cauți mai mult decât instrumente de dezvoltare? Aruncă o privire asupra browserului Firefox care este construit special pentru dezvoltatori și fluxuri de lucru moderne.">

<!-- LOCALIZATION NOTE (aboutDevtools.footer.learnMoreLink): Text for the link to
  -  https://www.mozilla.org/firefox/developer/ displayed in the footer. -->
<!ENTITY  aboutDevtools.footer.learnMoreLink "Află mai multe">


<!ENTITY  aboutDevtools.enable.inspectElementMessage
          "Examine and edit HTML and CSS with the Developer Tools’ Inspector.">
<!ENTITY  aboutDevtools.enable.aboutDebuggingMessage
          "Develop and debug WebExtensions, web workers, service workers and more with Firefox Developer Tools.">
<!ENTITY  aboutDevtools.enable.keyShortcutMessage
          "You activated a Developer Tools shortcut. If that was a mistake, you can close this Tab.">
<!ENTITY  aboutDevtools.enable.menuMessage2
          "Perfect your website’s HTML, CSS, and JavaScript with tools like Inspector and Debugger.">
<!ENTITY  aboutDevtools.enable.commonMessage
          "Firefox Developer Tools are disabled by default to give you more control over your browser.">
