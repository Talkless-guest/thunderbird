<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY aboutTelemetry.pingDataSource "Font de dades de ping:">
<!ENTITY aboutTelemetry.showCurrentPingData "Dades de ping actuals">
<!ENTITY aboutTelemetry.showArchivedPingData "Dades de ping arxivades">
<!ENTITY aboutTelemetry.showSubsessionData "Mostra dades de la subsessió">
<!ENTITY aboutTelemetry.choosePing "Trieu el ping:">
<!ENTITY aboutTelemetry.archivePingType "Tipus de ping">
<!ENTITY aboutTelemetry.archivePingHeader "Ping">
<!ENTITY aboutTelemetry.optionGroupToday "Avui">
<!ENTITY aboutTelemetry.optionGroupYesterday "Ahir">
<!ENTITY aboutTelemetry.optionGroupOlder "Més antic">
<!ENTITY aboutTelemetry.payloadChoiceHeader "  Càrrega útil">
<!-- LOCALIZATION NOTE(aboutTelemetry.previousPing, aboutTelemetry.nextPing):
	These strings are displayed when selecting Archived pings, and they’re
	used to move to the next or previous ping. -->
<!ENTITY aboutTelemetry.previousPing "&lt;&lt;">
<!ENTITY aboutTelemetry.nextPing "&gt;&gt;">

<!ENTITY aboutTelemetry.pageTitle "Dades de telemesura">
<!ENTITY aboutTelemetry.moreInformations "Voleu més informació?">


<!ENTITY aboutTelemetry.generalDataSection "  Dades generals">
<!ENTITY aboutTelemetry.environmentDataSection "  Dades de l'entorn">
<!ENTITY aboutTelemetry.sessionInfoSection "  Informació de la sessió">
<!ENTITY aboutTelemetry.scalarsSection "  Escalars">
<!ENTITY aboutTelemetry.keyedScalarsSection "  Escalars amb clau">
<!ENTITY aboutTelemetry.histogramsSection "  Histogrames">
<!ENTITY aboutTelemetry.keyedHistogramsSection "  Histogrames amb clau">
<!ENTITY aboutTelemetry.eventsSection "  Esdeveniments">
<!ENTITY aboutTelemetry.simpleMeasurementsSection "  Mesures senzilles">
<!ENTITY aboutTelemetry.telemetryLogSection "  Registre de telemesura">
<!ENTITY aboutTelemetry.slowSqlSection "  Sentències SQL lentes">
<!ENTITY aboutTelemetry.chromeHangsSection "  El navegador es penja">
<!ENTITY aboutTelemetry.addonDetailsSection "  Detalls del complement">
<!ENTITY aboutTelemetry.capturedStacksSection "  Piles capturades">
<!ENTITY aboutTelemetry.lateWritesSection "  Escriptures tardanes">
<!ENTITY aboutTelemetry.raw "JSON sense processar">

<!ENTITY aboutTelemetry.fullSqlWarning "  NOTA: la depuració de SQL lenta està habilitada. Es poden mostrar les cadenes SQL completes a sota però no s'enviaran per a la telemesura.">
<!ENTITY aboutTelemetry.fetchStackSymbols "  Obtén els noms de les funcions per a les piles">

<!ENTITY aboutTelemetry.firefoxDataDoc "The <a>Firefox Data Documentation</a> contains guides about how to work with our data tools.">
<!ENTITY aboutTelemetry.telemetryClientDoc "The <a>Firefox Telemetry client documentation</a> includes definitions for concepts, API documentation and data references.">
<!ENTITY aboutTelemetry.telemetryDashboard "The <a>Telemetry dashboards</a> allow you to visualize the data Mozilla receives via Telemetry.">
<!ENTITY aboutTelemetry.showInFirefoxJsonViewer "Open in the JSON viewer">
<!ENTITY aboutTelemetry.homeSection "Home">
<!ENTITY aboutTelemetry.rawPayloadSection "Raw Payload">
<!ENTITY aboutTelemetry.hideStackSymbols "Show raw stack data">
