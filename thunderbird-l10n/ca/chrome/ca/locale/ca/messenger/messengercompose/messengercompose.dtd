<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!--LOCALIZATION NOTE messengercompose.dtd Main UI for message composition -->
<!ENTITY msgComposeWindow.title "Redacta: (sense assumpte)">

<!-- File Menu -->
<!ENTITY fileMenu.label "Fitxer">
<!ENTITY fileMenu.accesskey "F">
<!ENTITY newMenu.label "Nou">
<!ENTITY newMenu.accesskey "N">
<!ENTITY newMessage.label "Missatge">
<!ENTITY newMessage.key "M">
<!ENTITY newMessageCmd2.key "N">
<!ENTITY newMessage.accesskey "M">
<!ENTITY newContact.label "Contacte de la llibreta d'adreces…">
<!ENTITY newContact.accesskey "C">
<!ENTITY attachMenu.label "Adjunta">
<!ENTITY attachMenu.accesskey "j">
<!ENTITY attachFileCmd.label "Fitxers…">
<!ENTITY attachFileCmd.accesskey "F">
<!ENTITY attachFileCmd.key "A">
<!ENTITY attachCloudCmd.label "Filelink">
<!ENTITY attachCloudCmd.accesskey "i">
<!ENTITY attachPageCmd.label "Pàgina web…">
<!ENTITY attachPageCmd.accesskey "w">
<!--LOCALIZATION NOTE attachVCardCmd.label Don't translate the term 'vCard' -->
<!ENTITY attachVCardCmd.label "Targeta personal (vCard)">
<!ENTITY attachVCardCmd.accesskey "p">
<!ENTITY remindLater.label "Recorda-m'ho més tard">
<!ENTITY remindLater.accesskey "R">
<!ENTITY closeCmd.label "Tanca">
<!ENTITY closeCmd.key "W">
<!ENTITY closeCmd.accesskey "c">
<!ENTITY saveCmd.label "Desa">
<!ENTITY saveCmd.key "S">
<!ENTITY saveCmd.accesskey "s">
<!ENTITY saveAsCmd.label "Anomena i desa">
<!ENTITY saveAsCmd.accesskey "A">
<!ENTITY saveAsFileCmd.label "Fitxer…">
<!ENTITY saveAsFileCmd.accesskey "F">
<!ENTITY saveAsDraftCmd.label "Esborrany">
<!ENTITY saveAsDraftCmd.accesskey "r">
<!ENTITY saveAsTemplateCmd.label "Plantilla">
<!ENTITY saveAsTemplateCmd.accesskey "t">
<!ENTITY sendNowCmd.label "Envia'l ara">
<!ENTITY sendCmd.keycode "VK_RETURN">
<!ENTITY sendNowCmd.accesskey "r">
<!ENTITY sendLaterCmd.label "Envia'l més tard">
<!ENTITY sendLaterCmd.keycode "VK_RETURN">
<!ENTITY sendLaterCmd.accesskey "l">
<!ENTITY printSetupCmd.label "Configuració de la pàgina…">
<!ENTITY printSetupCmd.accesskey "g">
<!ENTITY printPreviewCmd.label "Exemple d'impressió">
<!ENTITY printPreviewCmd.accesskey "l">
<!ENTITY printCmd.label "Imprimeix…">
<!ENTITY printCmd.key "P">
<!ENTITY printCmd.accesskey "p">

<!-- Edit Menu -->
<!ENTITY editMenu.label "Edita">
<!ENTITY editMenu.accesskey "E">
<!ENTITY undoCmd.label "Desfés">
<!ENTITY undoCmd.key "Z">
<!ENTITY undoCmd.accesskey "D">
<!ENTITY redoCmd.label "Refés">
<!ENTITY redoCmd.key "Y">
<!ENTITY redoCmd.accesskey "R">
<!ENTITY cutCmd.label "Retalla">
<!ENTITY cutCmd.key "X">
<!ENTITY cutCmd.accesskey "t">
<!ENTITY copyCmd.label "Copia">
<!ENTITY copyCmd.key "C">
<!ENTITY copyCmd.accesskey "C">
<!ENTITY pasteCmd.label "Enganxa">
<!ENTITY pasteCmd.key "V">
<!ENTITY pasteCmd.accesskey "x">
<!ENTITY pasteNoFormattingCmd.key "V">
<!ENTITY pasteAsQuotationCmd.key "o">
<!ENTITY editRewrapCmd.accesskey "j">
<!ENTITY deleteCmd.label "Suprimeix">
<!ENTITY deleteCmd.accesskey "u">
<!ENTITY editRewrapCmd.label "Torna a ajustar">
<!ENTITY editRewrapCmd.key "R">
<!ENTITY renameAttachmentCmd.label "Canvia el nom del fitxer adjunt...">
<!ENTITY renameAttachmentCmd.accesskey "e">
<!ENTITY toggleAttachmentPaneCmd.label "Subfinestra d'adjuncions">
<!-- LOCALIZATION NOTE (toggleAttachmentPaneCmd.accesskey):
     For better mnemonics, toggleAttachmentPaneCmd.accesskey should be the same
     as attachments.accesskey. -->
<!ENTITY toggleAttachmentPaneCmd.accesskey "a">
<!-- LOCALIZATION NOTE (toggleAttachmentPaneCmd.key):
     With OS-specific access modifier, this defines the shortcut key for showing/
     hiding/minimizing the attachment pane, e.g. Alt+M on Windows.
     toggleAttachmentPaneCmd.key and attachments.accesskey must be the same
     for the bucket view state machinery to work. -->
<!ENTITY toggleAttachmentPaneCmd.key "m">
<!ENTITY selectAllCmd.label "Selecciona-ho tot">
<!ENTITY selectAllCmd.key "A">
<!ENTITY selectAllCmd.accesskey "a">
<!ENTITY findBarCmd.label "Cerca…">
<!ENTITY findBarCmd.accesskey "C">
<!ENTITY findBarCmd.key "F">
<!ENTITY findReplaceCmd.label "Cerca i reemplaça…">
<!ENTITY findReplaceCmd.accesskey "l">
<!ENTITY findReplaceCmd.key "H">
<!ENTITY findAgainCmd.label "Torna a cercar">
<!ENTITY findAgainCmd.accesskey "o">
<!ENTITY findAgainCmd.key "G">
<!ENTITY findAgainCmd.key2 "VK_F3">
<!ENTITY findPrevCmd.label "Cerca l'anterior">
<!ENTITY findPrevCmd.accesskey "n">
<!ENTITY findPrevCmd.key "G">
<!ENTITY findPrevCmd.key2 "VK_F3">

<!-- Reorder Attachment Panel -->

<!-- LOCALIZATION NOTE (sortAttachmentsPanelBtn.Sort.AZ.label):
     Please ensure that this translation matches
     sortAttachmentsPanelBtn.Sort.ZA.label, except for the sort direction. -->
<!-- LOCALIZATION NOTE (sortAttachmentsPanelBtn.SortSelection.AZ.label):
     Please ensure that this translation matches
     sortAttachmentsPanelBtn.SortSelection.ZA.label, except for the sort direction. -->

<!-- View Menu -->
<!ENTITY viewMenu.label "Visualitza">
<!ENTITY viewMenu.accesskey "V">
<!ENTITY viewToolbarsMenuNew.label "Barres d'eines">
<!ENTITY viewToolbarsMenuNew.accesskey "B">
<!ENTITY menubarCmd.label "Barra de menú">
<!ENTITY menubarCmd.accesskey "m">
<!ENTITY showCompositionToolbarCmd.label "Barra de redacció">
<!ENTITY showCompositionToolbarCmd.accesskey "d">
<!ENTITY showFormattingBarCmd.label "Barra de format">
<!ENTITY showFormattingBarCmd.accesskey "f">
<!ENTITY showTaskbarCmd.label "Barra d'estat">
<!ENTITY showTaskbarCmd.accesskey "s">
<!ENTITY customizeToolbar.label "Personalitza…">
<!ENTITY customizeToolbar.accesskey "P">

<!ENTITY addressSidebar.label "Barra lateral de contactes">
<!ENTITY addressSidebar.accesskey "o">

<!-- Format Menu -->
<!ENTITY formatMenu.label "Format">
<!ENTITY formatMenu.accesskey "o">

<!-- Options Menu -->
<!ENTITY optionsMenu.label "Opcions">
<!ENTITY optionsMenu.accesskey "p">
<!ENTITY checkSpellingCmd2.label "Verifica l'ortografia…">
<!ENTITY checkSpellingCmd2.key "p">
<!ENTITY checkSpellingCmd2.key2 "VK_F7">
<!ENTITY checkSpellingCmd2.accesskey "o">
<!ENTITY enableInlineSpellChecker.label "Revisa l'ortografia a mesura que s'escriu">
<!ENTITY enableInlineSpellChecker.accesskey "s">
<!ENTITY quoteCmd.label "Cita el missatge">
<!ENTITY quoteCmd.accesskey "m">

<!--LOCALIZATION NOTE attachVCard.label Don't translate the term 'vCard' -->
<!ENTITY attachVCard.label "Adjunta una targeta personal (vCard)">
<!ENTITY attachVCard.accesskey "v">

<!ENTITY returnReceiptMenu.label "Confirmació de recepció">
<!ENTITY returnReceiptMenu.accesskey "r">
<!ENTITY dsnMenu.label "Notificació de l'estat del lliurament">
<!ENTITY dsnMenu.accesskey "N">
<!ENTITY deliveryFormatMenu.label "Format de lliurament">
<!ENTITY deliveryFormatMenu.accesskey "F">
<!ENTITY autoFormatCmd.label "Detecció automàtica">
<!ENTITY autoFormatCmd.accesskey "a">
<!ENTITY plainTextFormatCmd.label "Només text net">
<!ENTITY plainTextFormatCmd.accesskey "t">
<!ENTITY htmlFormatCmd.label "Només text enriquit (HTML)">
<!ENTITY htmlFormatCmd.accesskey "r">
<!ENTITY bothFormatCmd.label "Text net i enriquit (HTML)">
<!ENTITY bothFormatCmd.accesskey "L">
<!ENTITY priorityMenu.label "Prioritat">
<!ENTITY priorityMenu.accesskey "P">
<!ENTITY priorityButton.title "Prioritat">
<!ENTITY priorityButton.tooltiptext "Canvia la prioritat del missatge">
<!ENTITY priorityButton.label "Prioritat:">
<!ENTITY lowestPriorityCmd.label "Mínima">
<!ENTITY lowestPriorityCmd.accesskey "M">
<!ENTITY lowPriorityCmd.label "Baixa">
<!ENTITY lowPriorityCmd.accesskey "B">
<!ENTITY normalPriorityCmd.label "Normal">
<!ENTITY normalPriorityCmd.accesskey "N">
<!ENTITY highPriorityCmd.label "Alta">
<!ENTITY highPriorityCmd.accesskey "A">
<!ENTITY highestPriorityCmd.label "Màxima">
<!ENTITY highestPriorityCmd.accesskey "x">
<!ENTITY fileCarbonCopyCmd.label "Envia una còpia a">
<!ENTITY fileCarbonCopyCmd.accesskey "i">
<!ENTITY fileHereMenu.label "Arxiva'l aquí">

<!-- Tools Menu -->
<!ENTITY tasksMenu.label "Eines">
<!ENTITY tasksMenu.accesskey "n">
<!ENTITY messengerCmd.label "Missatgeria">
<!ENTITY messengerCmd.accesskey "M">
<!ENTITY messengerCmd.commandkey "1">
<!ENTITY addressBookCmd.label "Llibreta d'adreces">
<!ENTITY addressBookCmd.accesskey "a">
<!ENTITY addressBookCmd.key "B">
<!ENTITY accountManagerCmd2.label "Paràmetres del compte">
<!ENTITY accountManagerCmd2.accesskey "P">
<!ENTITY accountManagerCmdUnix2.accesskey "P">
<!ENTITY preferencesCmd2.label "Opcions">
<!ENTITY preferencesCmd2.accesskey "O">
<!ENTITY preferencesCmdUnix.label "Preferències">
<!ENTITY preferencesCmdUnix.accesskey "n">

<!--  Mac OS X Window Menu -->
<!ENTITY minimizeWindow.key "m">
<!ENTITY minimizeWindow.label "Minimitza la finestra">
<!ENTITY bringAllToFront.label "Porta-ho tot al davant">
<!ENTITY zoomWindow.label "Finestra del zoom">
<!ENTITY windowMenu.label "Finestra">

<!-- Mail Toolbar -->
<!ENTITY sendButton.label "Envia">
<!ENTITY quoteButton.label "Cita">
<!ENTITY addressButton.label "Contactes">
<!ENTITY attachButton.label "Adjunta">
<!ENTITY spellingButton.label "Ortografia">
<!ENTITY saveButton.label "Desa">
<!ENTITY printButton.label "Imprimeix">

<!-- Mail Toolbar Tooltips -->
<!ENTITY sendButton.tooltip "Envia ara aquest missatge">
<!ENTITY sendlaterButton.tooltip "Envia aquest missatge més tard">
<!ENTITY quoteButton.tooltip "Cita el missatge anterior">
<!ENTITY addressButton.tooltip "Seleccioneu un destinatari d'una Llibreta d'adreces">
<!ENTITY attachButton.tooltip "Adjunta un fitxer a aquest missatge">
<!ENTITY spellingButton.tooltip "Comprova l'ortografia de la selecció o del missatge sencer">
<!ENTITY saveButton.tooltip "Desa aquest missatge">
<!ENTITY cutButton.tooltip              "Retalla">
<!ENTITY copyButton.tooltip             "Copia">
<!ENTITY pasteButton.tooltip            "Enganxa">
<!ENTITY printButton.tooltip "Imprimeix aquest missatge">

<!-- Headers -->
<!--LOCALIZATION NOTE headersSpace.style is for aligning  the From:, To: and
    Subject: rows. It should be larger than the largest Header label  -->
<!ENTITY headersSpace.style "width: 9em;">
<!ENTITY fromAddr.label "De:">
<!ENTITY fromAddr.accesskey "D">
<!ENTITY toAddr.label "A:">
<!ENTITY ccAddr.label "Cc:">
<!ENTITY bccAddr.label "Cco:">
<!ENTITY replyAddr.label "Respon-A:">
<!ENTITY newsgroupsAddr.label "Grup de discussió:">
<!ENTITY followupAddr.label "Seguiment-A:">
<!ENTITY subject.label "Assumpte:">
<!ENTITY subject.accesskey "s">
<!-- LOCALIZATION NOTE (attachments.accesskey) This access key character should
     be taken from the strings in attachmentCount in composeMsgs.properties,
     and it must be the same as toggleAttachmentPaneCmd.key, otherwise it won't
     work, as the shortcut key functionality supersedes the access key. -->
<!ENTITY attachments.accesskey "j">


<!-- Format Toolbar, imported from editorAppShell.xul -->
<!ENTITY SmileButton.tooltip "Insereix una cara somrient">
<!ENTITY smiley1Cmd.label "Somriure">
<!ENTITY smiley2Cmd.label "Enfadat">
<!ENTITY smiley3Cmd.label "Fer l'ullet">
<!ENTITY smiley4Cmd.label "Llengua fora">
<!ENTITY smiley5Cmd.label "Riure">
<!ENTITY smiley6Cmd.label "Avergonyit">
<!ENTITY smiley7Cmd.label "Indecís">
<!ENTITY smiley8Cmd.label "Sorpresa">
<!ENTITY smiley9Cmd.label "Petó">
<!ENTITY smiley10Cmd.label "Crit">
<!ENTITY smiley11Cmd.label "Guai">
<!ENTITY smiley12Cmd.label "Boca de diners">
<!ENTITY smiley13Cmd.label "Peu a la boca">
<!ENTITY smiley14Cmd.label "Innocent">
<!ENTITY smiley15Cmd.label "Plor">
<!ENTITY smiley16Cmd.label "Llavis segellats">

<!-- Message Pane Context Menu -->
<!ENTITY spellCheckNoSuggestions.label "No s'ha trobat cap suggeriment">
<!ENTITY spellCheckIgnoreWord.label "Ignora la paraula">
<!ENTITY spellCheckIgnoreWord.accesskey "I">
<!ENTITY spellCheckAddToDictionary.label "Afegeix al diccionari">
<!ENTITY spellCheckAddToDictionary.accesskey "n">
<!ENTITY undo.label "Desfés">
<!ENTITY undo.accesskey "D">
<!ENTITY cut.label "Retalla">
<!ENTITY cut.accesskey "t">
<!ENTITY copy.label "Copia">
<!ENTITY copy.accesskey "C">
<!ENTITY paste.label "Enganxa">
<!ENTITY paste.accesskey "x">
<!ENTITY pasteQuote.label "Enganxa com a cita">
<!ENTITY pasteQuote.accesskey "i">

<!-- Attachment Item and List Context Menus -->
<!ENTITY openAttachment.label "Obre">
<!ENTITY openAttachment.accesskey "O">
<!ENTITY delete.label "Suprimeix">
<!ENTITY delete.accesskey "S">
<!ENTITY removeAttachment.label "Suprimeix l'adjunció">
<!ENTITY removeAttachment.accesskey "m">
<!ENTITY renameAttachment.label "Reanomena…">
<!ENTITY renameAttachment.accesskey "R">
<!ENTITY selectAll.label "Selecciona-ho tot">
<!ENTITY selectAll.accesskey "a">
<!ENTITY attachFile.label "Adjunta un fitxer…">
<!ENTITY attachFile.accesskey "f">
<!ENTITY attachCloud.label "Filelink…">
<!ENTITY attachCloud.accesskey "i">
<!ENTITY convertCloud.label "Converteix a…">
<!ENTITY convertCloud.accesskey "C">
<!ENTITY cancelUpload.label "Cancel·la la pujada">
<!ENTITY cancelUpload.accesskey "n">
<!ENTITY convertRegularAttachment.label "Adjunció regular">
<!ENTITY convertRegularAttachment.accesskey "A">
<!ENTITY attachPage.label "Adjunta una pàgina web…">
<!ENTITY attachPage.accesskey "w">

<!-- Attachment Pane Header Bar Context Menu -->
<!-- LOCALIZATION NOTE (initiallyShowAttachmentPane.label):
     Should use the same wording as startExpandedCmd.label
     in msgHdrViewOverlay.dtd. -->
<!ENTITY initiallyShowAttachmentPane.label "Inicialment mostra la subfinestra d'adjuncions">
<!ENTITY initiallyShowAttachmentPane.accesskey "m">

<!-- Spell checker context menu items -->
<!ENTITY spellAddDictionaries.label "Afegeix diccionaris…">
<!ENTITY spellAddDictionaries.accesskey "A">

<!-- Title for the address picker panel -->
<!ENTITY addressesSidebarTitle.label "Contactes">

<!-- Identity popup customize menuitem -->
<!ENTITY customizeFromAddress.label "Personalitza l'adreça «De»…">
<!ENTITY customizeFromAddress.accesskey "P">

<!-- Status Bar -->
<!ENTITY languageStatusButton.tooltip "Llengua de la verificació ortogràfica">
<!ENTITY encodingStatusPanel.tooltip "Codificació del text">

<!ENTITY reorderAttachmentsCmd.label "Reorder Attachments…">
<!ENTITY reorderAttachmentsCmd.accesskey "s">
<!ENTITY reorderAttachmentsCmd.key "x">
<!ENTITY reorderAttachmentsPanel.label "Reorder Attachments">
<!ENTITY moveAttachmentTopPanelBtn.label "Move to Top">
<!ENTITY moveAttachmentUpPanelBtn.label "Move Up">
<!ENTITY moveAttachmentBundleUpPanelBtn.label "Move together">
<!ENTITY moveAttachmentDownPanelBtn.label "Move Down">
<!ENTITY moveAttachmentBottomPanelBtn.label "Move to Bottom">
<!ENTITY sortAttachmentsPanelBtn.Sort.AZ.label "Sort: A - Z">
<!ENTITY sortAttachmentsPanelBtn.Sort.ZA.label "Sort: Z - A">
<!ENTITY sortAttachmentsPanelBtn.SortSelection.AZ.label "Sort Selection: A - Z">
<!ENTITY sortAttachmentsPanelBtn.SortSelection.ZA.label "Sort Selection: Z - A">
<!ENTITY sortAttachmentsPanelBtn.key "y">
<!ENTITY attachButton.tooltip2 "Add an attachment">
<!ENTITY attachmentBucketHeader.tooltip "Attachment Pane">
<!ENTITY attachmentBucketCloseButton.tooltip "Hide the attachment pane">
<!ENTITY reorderAttachments.label "Reorder Attachments…">
<!ENTITY reorderAttachments.accesskey "s">
<!ENTITY removeAllAttachments.label "Remove All Attachments">
<!ENTITY removeAllAttachments.accesskey "v">
