# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE(pageSubtitle):
# - %1$S is replaced by the value of the toolkit.telemetry.server_owner preference
# - %2$S is replaced by brandFullName
pageSubtitle = Šiame tinklalapyje rasite telemetrijai sukauptus duomenis apie kompiuterio aparatinę įrangą, programos našumą, tinklinimą ir naudojamas fukcijas. Ši informacija programos „%2$S“ tobulinimo tikslais siunčiama į „%1$S“.

# LOCALIZATION NOTE(homeExplanation):
# - %1$S is either telemetryEnabled or telemetryDisabled
# - %2$S is either extendedTelemetryEnabled or extendedTelemetryDisabled
homeExplanation = Telemetrija yra %1$S, išplėstinė telemetrija yra %2$S.
telemetryEnabled = aktyvi
telemetryDisabled = išjungta
extendedTelemetryEnabled = aktyvi
extendedTelemetryDisabled = išjungta

# LOCALIZATION NOTE(settingsExplanation):
# - %1$S is either releaseData or prereleaseData
# - %2$S is either telemetryUploadEnabled or telemetryUploadDisabled
settingsExplanation = Telemetrija renka %1$S, o jų perdavimas yra %2$S.
releaseData = laidos duomenis
prereleaseData = išankstinės laidos duomenis
telemetryUploadEnabled = įjungtas
telemetryUploadDisabled = išjungtas

# LOCALIZATION NOTE(pingDetails):
# - %1$S is replaced by a link with pingExplanationLink as text
# - %2$S is replaced by namedPing
pingDetails = Kiekviena informacijos dalis yra siunčiama sugrupuota į „%1$S“. Dabar žiūrite į %2$S ryšio patikrinimą.
# LOCALIZATION NOTE(namedPing):
# - %1$S is replaced by the ping localized timestamp, e.g. “2017/07/08 10:40:46”
# - %2$S is replaced by the ping name, e.g. “saved-session”
namedPing = %1$S, „%2$S“
# LOCALIZATION NOTE(pingDetailsCurrent):
# - %1$S is replaced by a link with pingExplanationLink as text
# - %2$S is replaced by currentPing
pingDetailsCurrent = Kiekviena informacijos dalis yra siunčiama sugrupuota į „%1$S“. Dabar žiūrite į %2$S ryšio patikrinimą.
pingExplanationLink = ryšio patikrinimus
currentPing = dabartinį

# LOCALIZATION NOTE(filterPlaceholder): string used as a placeholder for the
# search field, %1$S is replaced by the section name from the structure of the
# ping. More info about it can be found here:
# https://firefox-source-docs.mozilla.org/toolkit/components/telemetry/telemetry/data/main-ping.html
filterPlaceholder = Rasti tarp %1$S
filterAllPlaceholder = Rasti visuose skyriuose

# LOCALIZATION NOTE(resultsForSearch): %1$S is replaced by the searched terms
resultsForSearch = Rezultatai ieškant „%1$S“
# LOCALIZATION NOTE(noSearchResults):
# - %1$S is replaced by the section name from the structure of the ping.
# More info about it can be found here: https://firefox-source-docs.mozilla.org/toolkit/components/telemetry/telemetry/data/main-ping.html
# - %2$S is replaced by the current text in the search input
noSearchResults = Deja! Skyriuje „%1$S“ nėra rezultatų, atitinkančių „%2$S“
# LOCALIZATION NOTE(noSearchResultsAll): %S is replaced by the searched terms
noSearchResultsAll = Deja! Nė viename skyriuje nėra rezultatų, atitinkančių „%S“
# LOCALIZATION NOTE(noDataToDisplay): %S is replaced by the section name.
# This message is displayed when a section is empty.
noDataToDisplay = Deja! Skyriuje „%S“ šiuo metu duomenų nėra
# LOCALIZATION NOTE(currentPingSidebar): used as a tooltip for the “current”
# ping title in the sidebar
currentPingSidebar = dabartinis ryšio patikrinimas
# LOCALIZATION NOTE(telemetryPingTypeAll): used in the “Ping Type” select
telemetryPingTypeAll = visi

# LOCALIZATION NOTE(histogram*): these strings are used in the “Histograms” section
histogramSamples = mėginiai (-ių)
histogramAverage = vidurkis
histogramSum = suma
# LOCALIZATION NOTE(histogramCopy): button label to copy the histogram
histogramCopy = Kopijuoti

# LOCALIZATION NOTE(telemetryLog*): these strings are used in the “Telemetry Log” section
telemetryLogTitle = Telemetrijos žurnalas
telemetryLogHeadingId = Id
telemetryLogHeadingTimestamp = Laiko žymė
telemetryLogHeadingData = Kita

# LOCALIZATION NOTE(slowSql*): these strings are used in the “Slow SQL Statements” section
slowSqlMain = Lėti SQL sakiniai pagrindinėje gijoje
slowSqlOther = Lėti SQL sakiniai pagalbinėse gijose
slowSqlHits = Pasitaikymai
slowSqlAverage = Vid. trukmė (ms)
slowSqlStatement = Sakinys

# LOCALIZATION NOTE(histogram*): these strings are used in the “Add-on Details” section
addonTableID = Priedo identifikatorius
addonTableDetails = Savybės
# LOCALIZATION NOTE(addonProvider):
# - %1$S is replaced by the name of an Add-on Provider (e.g. “XPI”, “Plugin”)
addonProvider = Tipas: „%1$S“

keysHeader = Savybė
namesHeader = Pavadinimas
valuesHeader = Reikšmė

# LOCALIZATION NOTE(chrome-hangs-title):
# - %1$S is replaced by the number of the hang
# - %2$S is replaced by the duration of the hang
chrome-hangs-title = Strigties pranešimas Nr. %1$S (%2$S sek.)
# LOCALIZATION NOTE(captured-stacks-title):
# - %1$S is replaced by the string key for this stack
# - %2$S is replaced by the number of times this stack was captured
captured-stacks-title = „%1$S“ (įrašymų skaičius: %2$S)
# LOCALIZATION NOTE(late-writes-title):
# - %1$S is replaced by the number of the late write
late-writes-title = Vėlavęs įrašymas Nr. %1$S

stackTitle = Dėklas:
memoryMapTitle = Atminties planas:

errorFetchingSymbols = Gaunant simbolius įvyko klaida. Įsitikinę, kad esate prisijungę prie interneto, pabandykite dar kartą.

parentPayload = Išorinis turinys
# LOCALIZATION NOTE(childPayloadN):
# - %1$S is replaced by the number of the child payload (e.g. “1”, “2”)
childPayloadN = Vidinis turinys Nr. %1$S
timestampHeader = laikas
categoryHeader = kategorija
methodHeader = metodas
objectHeader = objektas
extraHeader = papildomai
