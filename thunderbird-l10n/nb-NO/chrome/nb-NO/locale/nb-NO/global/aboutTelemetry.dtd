<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY aboutTelemetry.pingDataSource "Pingdata-kilde:">
<!ENTITY aboutTelemetry.showCurrentPingData "Gjeldende ping-data">
<!ENTITY aboutTelemetry.showArchivedPingData "Arkiverte ping-data">
<!ENTITY aboutTelemetry.showSubsessionData "Vis undersøkt-data">
<!ENTITY aboutTelemetry.choosePing "Velg ping:">
<!ENTITY aboutTelemetry.archivePingType "
Ping-type
">
<!ENTITY aboutTelemetry.archivePingHeader "Ping">
<!ENTITY aboutTelemetry.optionGroupToday "
I dag
">
<!ENTITY aboutTelemetry.optionGroupYesterday "
I går
">
<!ENTITY aboutTelemetry.optionGroupOlder "
Eldre
">
<!ENTITY aboutTelemetry.payloadChoiceHeader "  Nyttelast">
<!-- LOCALIZATION NOTE(aboutTelemetry.previousPing, aboutTelemetry.nextPing):
	These strings are displayed when selecting Archived pings, and they’re
	used to move to the next or previous ping. -->
<!ENTITY aboutTelemetry.previousPing "&lt;&lt;">
<!ENTITY aboutTelemetry.nextPing "&gt;&gt;">

<!ENTITY aboutTelemetry.pageTitle "Telemetri-data">
<!ENTITY aboutTelemetry.moreInformations "
Ser du etter mer informasjon?
">
<!ENTITY aboutTelemetry.firefoxDataDoc "
<a>Firefox Data Documentation</a> inneholder veiledninger om hvordan du jobber med dataverktøyene våre.
">
<!ENTITY aboutTelemetry.telemetryClientDoc "
<a>Firefox Telemetry-klientdokumentasjonen</a> inneholder definisjoner for konsepter, API-dokumentasjon og datareferanser.
">
<!ENTITY aboutTelemetry.telemetryDashboard "
Med <a>Telemetry-panelet</a> kan du visualisere dataene Mozilla mottar via Telementry.
">

<!ENTITY aboutTelemetry.showInFirefoxJsonViewer "
Åpne i JSON-viser
">

<!ENTITY aboutTelemetry.homeSection "Hjem">
<!ENTITY aboutTelemetry.generalDataSection "  Generell data">
<!ENTITY aboutTelemetry.environmentDataSection "  Miljødata">
<!ENTITY aboutTelemetry.sessionInfoSection "  Øktinformasjon">
<!ENTITY aboutTelemetry.scalarsSection "  Skalarer">
<!ENTITY aboutTelemetry.keyedScalarsSection "  Keyed Scalars">
<!ENTITY aboutTelemetry.histogramsSection "  Histogrammer">
<!ENTITY aboutTelemetry.keyedHistogramsSection "  Histogrammer etter nøkler">
<!ENTITY aboutTelemetry.eventsSection "  hendelser">
<!ENTITY aboutTelemetry.simpleMeasurementsSection "  Enkle målinger">
<!ENTITY aboutTelemetry.telemetryLogSection "  Telemetri-logg">
<!ENTITY aboutTelemetry.slowSqlSection "  Trege SQL-uttrykk">
<!ENTITY aboutTelemetry.chromeHangsSection "  Nettleserheng">
<!ENTITY aboutTelemetry.addonDetailsSection "  Utvidelsesdetaljer">
<!ENTITY aboutTelemetry.capturedStacksSection "  oppfanget stakker">
<!ENTITY aboutTelemetry.lateWritesSection "  Sen skriving">
<!ENTITY aboutTelemetry.rawPayloadSection "
Rå nyttelast
">
<!ENTITY aboutTelemetry.raw "Rå JSON">

<!ENTITY aboutTelemetry.fullSqlWarning "  MERK: Treg SQL-feilsøking er påslått. Fullstendige SQL-uttrykk kan vises nedenfor, men de sendes ikke inn til Telemetry.">
<!ENTITY aboutTelemetry.fetchStackSymbols "
  Hent funksjonsnavn for stacker
">
<!ENTITY aboutTelemetry.hideStackSymbols "
  Vis rå stackdata
">
