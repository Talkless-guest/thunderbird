<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY itemGeneral.label       "عام">
<!ENTITY dataChoicesTab.label    "اختيارات البيانات">
<!ENTITY itemUpdate.label        "التحديث">
<!ENTITY itemNetworking.label    "الشبكة و مساحة القرص">
<!ENTITY itemCertificates.label  "الشهادات">

<!-- General Settings -->
<!ENTITY enableGlodaSearch.label       "فعّل البحث العمومي والفهرسة">
<!ENTITY enableGlodaSearch.accesskey   "ف">
<!ENTITY dateTimeFormatting.label      "تنسيق التاريخ و الوقت">
<!ENTITY allowHWAccel.label            "استخدم تسريع العتاد إن كان متاحًا">
<!ENTITY allowHWAccel.accesskey        "ع">
<!ENTITY storeType.label               "طريقة تخزين الرسائل للحسابات الجديدة:">
<!ENTITY storeType.accesskey           "خ">
<!ENTITY mboxStore2.label              "ملف لكل مجلد (mbox)">
<!ENTITY maildirStore.label            "ملف لكل رسالة (maildir)">

<!ENTITY scrolling.label               "اللف">
<!ENTITY useAutoScroll.label           "استخدم اللّف الآلي">
<!ENTITY useAutoScroll.accesskey       "ف">
<!ENTITY useSmoothScrolling.label      "استخدم اللّف السلس">
<!ENTITY useSmoothScrolling.accesskey  "خ">

<!ENTITY systemIntegration.label       "تكامل النظام">
<!ENTITY alwaysCheckDefault.label      "تحقق دائمًا عند البدء إن كان &brandShortName; هو عميل البريد الافتراضي">
<!ENTITY alwaysCheckDefault.accesskey  "ئ">
<!ENTITY searchIntegration.label       "اسمح ل‍ &searchIntegration.engineName; بالبحث في الرسائل">
<!ENTITY searchIntegration.accesskey   "ث">
<!ENTITY checkDefaultsNow.label        "التمس الآن…">
<!ENTITY checkDefaultsNow.accesskey    "ت">
<!ENTITY configEditDesc.label          "الإعدادات المتقدمة">
<!ENTITY configEdit.label              "محرر الإعدادات…">
<!ENTITY configEdit.accesskey          "ت">
<!ENTITY returnReceiptsInfo.label      "حدد كيف يتعامل &brandShortName; مع إعلامات الوصول">
<!ENTITY showReturnReceipts.label      "إعلامات الوصول…">
<!ENTITY showReturnReceipts.accesskey  "ع">

<!-- Data Choices -->
<!ENTITY telemetrySection.label          "تليمتري">
<!ENTITY telemetryDesc.label             "شارك بيانات عميل البريد فيما يتعلق بالأداء و الاستخدام و العتاد و التخصيصات مع &vendorShortName; لمساعدتنا في جعل &brandShortName; أفضل">
<!ENTITY enableTelemetry.label           "فعّل أداة تليمتري">
<!ENTITY enableTelemetry.accesskey       "ت">
<!ENTITY telemetryLearnMore.label        "اطّلع على المزيد">

<!ENTITY crashReporterSection.label      "مُبلّغ الانهيار">
<!ENTITY crashReporterDesc.label         "يُرسل &brandShortName; بلاغات الانهيار إلى &vendorShortName; للمساعدة في تحسين أداء عميل البريد و استقراره">
<!ENTITY enableCrashReporter.label       "فعّل مبلّغ الانهيار">
<!ENTITY enableCrashReporter.accesskey   "ن">
<!ENTITY crashReporterLearnMore.label    "اطّلع على المزيد">

<!-- Update -->
<!-- LOCALIZATION NOTE (updateApp.label):
  Strings from aboutDialog.dtd are displayed in this section of the preferences.
  Please check for possible accesskey conflicts.
-->
<!ENTITY updateApp2.label                "تحديثات &brandShortName;">
<!-- LOCALIZATION NOTE (updateApp.version.*): updateApp.version.pre is
  followed by a version number, keep the trailing space or replace it with
  a different character as needed. updateApp.version.post is displayed after
  the version number, and is empty on purpose for English. You can use it
  if required by your language.
 -->
<!ENTITY updateApp.version.pre           "الإصدارة ">
<!ENTITY updateApp.version.post          "">
<!ENTITY updateAuto.label                "نصّب التحديثات آليًا (مستحسن: يزيد الأمان)">
<!ENTITY updateAuto.accesskey            "ل">
<!ENTITY updateCheck.label               "التمس التحديثات، و لكن اترك لي خيار تنصيبها من عدمه">
<!ENTITY updateCheck.accesskey           "ت">
<!ENTITY updateManual.label              "لا تلتمس التحديثات أبدًا (غير مستحسن: مجازفة أمنية)">
<!ENTITY updateManual.accesskey          "د">
<!ENTITY updateHistory.label             "أظهر تأريخ التحديث">
<!ENTITY updateHistory.accesskey         "ظ">

<!ENTITY useService.label                "استخدم خدمة تعمل في الخلفية لتنصيب التحديثات">
<!ENTITY useService.accesskey            "خ">

<!-- Networking and Disk Space -->
<!ENTITY showSettings.label            "إعدادات…">
<!ENTITY showSettings.accesskey        "د">
<!ENTITY proxiesConfigure.label        "اضبط كيفية اتصال &brandShortName; بالإنترنت">
<!ENTITY connectionsInfo.caption       "الاتصال">
<!ENTITY offlineInfo.caption           "غير متّصل">
<!ENTITY offlineInfo.label             "اضبط إعدادات طور اللا اتصال">
<!ENTITY showOffline.label             "غير متصل…">
<!ENTITY showOffline.accesskey         "ص">

<!ENTITY Diskspace                       "مساحة القرص">
<!ENTITY offlineCompactFolders.label     "ادمج كل المجلدات إذا كانت ستوفِّر أكثر من">
<!ENTITY offlineCompactFolders.accesskey "و">
<!ENTITY offlineCompactFoldersMB.label   "م.بايت">

<!-- LOCALIZATION NOTE:
  The entities useCacheBefore.label and useCacheAfter.label appear on a single
  line in preferences as follows:

  &useCacheBefore.label  [ textbox for cache size in MB ]   &useCacheAfter.label;
-->
<!ENTITY useCacheBefore.label            "استخدم حتى">
<!ENTITY useCacheBefore.accesskey        "ى">
<!ENTITY useCacheAfter.label             "م.بايت من المساحة للذاكرة المخبأة">
<!ENTITY overrideSmartCacheSize.label    "تخطَّ الإدارة الآلية للذاكرة الخبيئة">
<!ENTITY overrideSmartCacheSize.accesskey "ذ">
<!ENTITY clearCacheNow.label             "امسح الآن">
<!ENTITY clearCacheNow.accesskey         "ح">

<!-- Certificates -->
<!ENTITY certSelection.description       "عندما يطلب خادوم شهادتي الشخصية:">
<!ENTITY certs.auto                      "اختر واحدة تلقائيًا">
<!ENTITY certs.auto.accesskey            "خ">
<!ENTITY certs.ask                       "اسألني كل مرة">
<!ENTITY certs.ask.accesskey             "ك">
<!ENTITY enableOCSP.label                "استعلم من خواديم مستجيبي OCSP عن الصلاحية الحالية للشهادات">
<!ENTITY enableOCSP.accesskey            "س">

<!ENTITY manageCertificates.label "أدِر الشهادات">
<!ENTITY manageCertificates.accesskey "ش">
<!ENTITY viewSecurityDevices.label "أجهزة الأمن">
<!ENTITY viewSecurityDevices.accesskey "م">
