<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- WARNING! This file contains UTF-8 encoded characters!
   - If this ==> … <== doesn't look like an ellipsis (three dots in a row),
   - your editor isn't using UTF-8 encoding and may munge up the document!
  -->

<!-- Tools menu -->
<!ENTITY lightning.preferencesLabel "Deiziataer">

<!-- New menu popup in File menu -->
<!ENTITY lightning.menupopup.new.event.label    "Darvoud...">
<!ENTITY lightning.menupopup.new.event.accesskey "D">
<!ENTITY lightning.menupopup.new.task.label     "Trevell...">
<!ENTITY lightning.menupopup.new.task.accesskey "T">
<!ENTITY lightning.menupopup.new.calendar.label "Deiziataer…">
<!ENTITY lightning.menupopup.new.calendar.accesskey "D">

<!-- Open menu popup in File menu -->
<!ENTITY lightning.menupopup.open.label               "Digeriñ">
<!ENTITY lightning.menupopup.open.accesskey           "D">
<!ENTITY lightning.menupopup.open.message.label       "Kemennadennoù enrollet...">
<!ENTITY lightning.menupopup.open.message.accesskey   "K">
<!ENTITY lightning.menupopup.open.calendar.label      "Restr deiziataer...">
<!ENTITY lightning.menupopup.open.calendar.accesskey  "R">

<!-- View Menu -->
<!ENTITY lightning.menu.view.calendar.label            "Deiziataer">
<!ENTITY lightning.menu.view.calendar.accesskey        "i">
<!ENTITY lightning.menu.view.tasks.label               "Trevelloù">
<!ENTITY lightning.menu.view.tasks.accesskey           "e">

<!-- Events and Tasks menu -->
<!ENTITY lightning.menu.eventtask.label                "Darvoudoù ha trevelloù">
<!ENTITY lightning.menu.eventtask.accesskey            "h">

<!-- properties dialog, calendar creation wizard -->
<!-- LOCALIZATON NOTE(lightning.calendarproperties.email.label,
     lightning.calendarproperties.forceEmailScheduling.label)
     These strings are used in the calendar wizard and the calendar properties dialog, but are only
     displayed when setting/using a caldav calendar -->
<!ENTITY lightning.calendarproperties.email.label                           "Postel :">

<!-- LOCALIZATON NOTE(lightning.calendarproperties.forceEmailScheduling.tooltiptext1,
     lightning.calendarproperties.forceEmailScheduling.tooltiptext2)
     - tooltiptext1 is used in the calendar wizard when setting a new caldav calendar
     - tooltiptext2 is used in the calendar properties dialog for caldav calendars -->

<!-- iMIP Bar (meeting support) -->
<!ENTITY lightning.imipbar.btnAccept.label                                  "Asantiñ">
<!ENTITY lightning.imipbar.btnAccept2.tooltiptext                           "Asantiñ ar bedadenn darvoud">
<!ENTITY lightning.imipbar.btnAcceptRecurrences.label                       "Asantiñ an holl">
<!ENTITY lightning.imipbar.btnAcceptRecurrences2.tooltiptext                "Asantiñ ar bedadenn zarvoud evit an holl reveziadennoù eus an darvoud">
<!ENTITY lightning.imipbar.btnAdd.label                                     "Ouzhpennañ">
<!ENTITY lightning.imipbar.btnAdd.tooltiptext                               "Ouzhpennañ un darvoud d'an deiziataer">
<!ENTITY lightning.imipbar.btnDecline2.tooltiptext                          "Nac'h ar bedadenn zarvoud">
<!ENTITY lightning.imipbar.btnDeclineRecurrences.label                      "Nac'hañ an holl">
<!ENTITY lightning.imipbar.btnDeclineRecurrences2.tooltiptext               "Nac'h ar bedadenn zarvoud evit an holl reveziadennoù eus an darvoud">
<!ENTITY lightning.imipbar.btnDelete.label                                  "Dilemel">
<!ENTITY lightning.imipbar.btnDelete.tooltiptext                            "Dilemel eus an deiziataer">
<!ENTITY lightning.imipbar.btnDetails.label                                 "Munudoù...">
<!ENTITY lightning.imipbar.btnDetails.tooltiptext                           "Diskouez munudoù an darvoud">
<!ENTITY lightning.imipbar.btnMore.label                                    "Muioc'h">
<!ENTITY lightning.imipbar.btnMore.tooltiptext                              "Klikit evit kaout muioc'h a zibarzhioù">
<!ENTITY lightning.imipbar.btnReconfirm2.label                              "Adkadarnaat">
<!ENTITY lightning.imipbar.btnReconfirm.tooltiptext                         "Kas un adkadarnadur d'an aozer">
<!ENTITY lightning.imipbar.btnSaveCopy.label                                "Enrollañ un eilad">
<!ENTITY lightning.imipbar.btnSaveCopy.tooltiptext                          "Enrollañ un eilad eus an darvoud war an deiziataer, distag eus ur respont d'an aozer. Roll ar berzhidi a vo goullonderet.">
<!ENTITY lightning.imipbar.btnTentative2.tooltiptext                        "Asantiñ ar bedadenn zarvoud evit ar mare">
<!ENTITY lightning.imipbar.btnTentativeRecurrences2.tooltiptext             "Asantiñ ar bedadenn zarvoud evit holl reveziadennoù an darvoud evit ar mare">
<!ENTITY lightning.imipbar.btnUpdate.label                                  "Hizivaat">
<!ENTITY lightning.imipbar.btnUpdate.tooltiptext                            "Hizivaat an darvoud en deiziataer">
<!ENTITY lightning.imipbar.description                                      "Ur bedadenn d'un darvoud a zo endalc'het er gemennadenn-mañ.">

<!-- Lightning specific keybindings -->

<!-- Account Central page -->
<!ENTITY lightning.acctCentral.newCalendar.label "Krouiñ un deiziataer nevez">

<!-- today-pane-specific -->
<!ENTITY todaypane.showTodayPane.label "Diskouez Panell Hiziv">
<!ENTITY todaypane.showTodayPane.accesskey "D">
<!ENTITY todaypane.statusButton.label "Panell &amp;hiziv">

<!ENTITY lightning.calendarproperties.forceEmailScheduling.label            "Prefer client-side email scheduling">
<!ENTITY lightning.calendarproperties.forceEmailScheduling.tooltiptext1     "For now, you can only enable this after setting up this calendar in its property dialog if the calendar server takes care of scheduling.">
<!ENTITY lightning.calendarproperties.forceEmailScheduling.tooltiptext2     "This option is only available if the calendar server handles scheduling. Enabling will allow to fall back to the standard email based scheduling instead of leaving it to the server.">
<!ENTITY lightning.imipbar.btnDecline.label                                 "Decline">
<!ENTITY lightning.imipbar.btnDeclineCounter.label                          "Decline">
<!ENTITY lightning.imipbar.btnDeclineCounter.tooltiptext                    "Decline the counter proposal">
<!ENTITY lightning.imipbar.btnReschedule.label                              "Reschedule">
<!ENTITY lightning.imipbar.btnReschedule.tooltiptext                        "Reschedule the event">
<!ENTITY lightning.imipbar.btnTentative.label                               "Tentative">
<!ENTITY lightning.imipbar.btnTentativeRecurrences.label                    "Tentative all">
<!ENTITY lightning.imipbar.btnSend.label                                    "Send a response now">
<!ENTITY lightning.imipbar.btnSend.tooltiptext                              "Send a response to the organizer">
<!ENTITY lightning.imipbar.btnSendSeries.tooltiptext                        "Send a response for the entire series to the organizer">
<!ENTITY lightning.imipbar.btnDontSend.label                                "Do not send a response">
<!ENTITY lightning.imipbar.btnDontSend.tooltiptext                          "Change your participation status without sending a response to the organizer">
<!ENTITY lightning.imipbar.btnDontSendSeries.tooltiptext                    "Change your participation status for the series without sending a response to the organizer">
<!ENTITY lightning.keys.event.showCalendar.key "C">
<!ENTITY lightning.keys.event.showTasks.key "D">
<!ENTITY lightning.keys.event.new "I">
<!ENTITY lightning.keys.todo.new "D">
<!ENTITY todaypane.showMinimonth.label "Show Mini-Month">
<!ENTITY todaypane.showMinimonth.accesskey "M">
<!ENTITY todaypane.showMiniday.label "Show Mini-Day">
<!ENTITY todaypane.showMiniday.accesskey  "d">
<!ENTITY todaypane.showNone.label "Show None">
<!ENTITY todaypane.showNone.accesskey "N">
